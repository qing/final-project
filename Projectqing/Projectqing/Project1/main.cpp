//#include <fstream>
//#include <string>
//#include<math.h>
//#include <cstdlib>
//#include "function.h"
//#include "readfile.h"
#include <iostream>
#include <vector>
#include "function.h"
#include "Implement.h"

using namespace std;


int main() // solve the zero point for polynomial function with different implementations
{


	vector<float> v_read = Read_File_txt("/home/qing/myfiles/Programmation/pretest/Projectqing/Projectqing/Project1/test.txt"); // choose between reading a txt or csv file
	//vector<float> v_read = Read_File_csv("test01.csv");

	//Imple_chord r1(v_read); // choose between five different method
	//Imple_bisection r1(v_read);
	//Imple_newton r1(v_read);
	//Imple_fixed_point r1(v_read);
	Imple_fixed_point_aitken r1(v_read);
	
	r1.func();
	r1.get_output();
	//system("pause");
	return 0;
}

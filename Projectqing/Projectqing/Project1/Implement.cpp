#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include<math.h>
#include <cstdlib>
#include "function.h"
#include "readfile.h"
#include "Implement.h"
using namespace std;

Imple::Imple(const vector<float> v_m_read)
{
	this->v_read = v_m_read;
	cout << "The read content is " << endl;
	printVector(v_read);
	v_input = function_input(v_read);
	int n = v_input.size();
	// Split v_input, one vector is placed on the left point, and the other vector is placed on the right point
	for (int i = 0; i < n / 2; i++)
	{
		v_input1.push_back(v_input[2 * i]);
		v_input2.push_back(v_input[2 * i + 1]);
	}
	cout << "the result for input is: " << endl;
	printVector(v_input1);
	printVector(v_input2);
	v_reinput = bisection_for_reinput(v_read, v_input1, v_input2);
	int m = v_reinput.size();
	for (int i = 0; i < m / 2; i++)
	{
		v_reinput1.push_back(v_reinput[2 * i]);
		v_reinput2.push_back(v_reinput[2 * i + 1]);
	}
	cout << "the result for reinput is: " << endl;
	printVector(v_reinput1);
	printVector(v_reinput2);
}


void Imple_bisection::func()
{
	this->v_out = bisection(v_read, v_reinput1, v_reinput2);
}

void Imple_bisection::get_output()
{
	printVector(v_out);
	output_csv(v_out);
}

void Imple_chord::func()
{
	this->v_out = Classic_chord_method(v_read, v_reinput1, v_reinput2);
}

void Imple_chord::get_output()
{
	printVector(v_out);
	output_csv(v_out);
}

void Imple_newton::func()
{
	this->v_out = Newton_method(v_read, v_reinput1, v_reinput2);
}

void Imple_newton::get_output()
{
	printVector(v_out);
	output_csv(v_out);
}

void Imple_fixed_point::func()
{
	this->v_out = fixed_point_method(v_read, v_reinput1, v_reinput2);
}

void Imple_fixed_point::get_output()
{
	printVector(v_out);
	output_csv(v_out);
}

void Imple_fixed_point_aitken::func()
{
	this->v_out = aitken_with_fixedpoint_method(v_read, v_reinput1, v_reinput2);
}

void Imple_fixed_point_aitken::get_output()
{
	printVector(v_out);
	output_csv(v_out);
}
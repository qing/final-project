#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "readfile.h"
using namespace std;


ReadFile_txt::ReadFile_txt(const string path)
{
	this->file_path = path;
}


vector<float> ReadFile_txt::read()
{
	vector<float> s;
	vector<float> k;
	k.push_back(0);
	ifstream ifs;
	ifs.open(file_path, ios::in);
	if (!ifs.is_open())
	{
		cout << "failed to open the file: " << file_path << endl;
		return k; //If we fail to open the file, it will return a vector contains zero
	}
	for (float a; ifs >> a;)
	{
		s.push_back(a); // We put the number we read to a vector
	}
	ifs.close();

	return s;
	}


ReadFile_csv::ReadFile_csv(const string path)
{
	this->file_path = path;
}


vector<float> ReadFile_csv::read()
{
	vector<float> s;
	vector<float> k;
	k.push_back(0);
	string line, number;
	ifstream f(file_path);
	if (f.fail())
	{
		cout << "failed to open the file: " << file_path << endl;
		return k;
	}
	while (std::getline(f, line))
	{
		istringstream is(line);
		while (getline(is, number, ','))
		{
			float x;
			stringstream sx;
			sx << number;
			sx >> x;
			s.push_back(x);
			//cout << number << " ";
			//cout << endl;
		}
	}
	f.close();
	return s;
}
#pragma once
#ifndef __FUNCTION__H__
#define __FUNCTION__H__
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include<math.h>
#include <cstdlib>


using namespace std;

float f(vector<float> v, int n, float x);

float derivative_f(vector<float> v, int n, float x);

void myswap(float* a, float* b);

void printVector(vector<float>& v);

vector<float> Read_File_txt(string file_path);

vector<float> Read_File_csv(string file_path);

void output_csv(vector<float>& v);

vector<float> function_input(vector<float>& v);

vector<float> bisection_for_reinput(vector<float>& v, vector<float>& v1, vector<float>& v2);

vector<float> bisection(vector<float>& v, vector<float>& v1, vector<float>& v2);

vector<float> Classic_chord_method(vector<float>& v, vector<float>& v1, vector<float>& v2);

vector<float> Newton_method(vector<float>& v, vector<float>& v1, vector<float>& v2);

vector<float> fixed_point_method(vector<float>& v, vector<float>& v1, vector<float>& v2);

vector<float> aitken_with_fixedpoint_method(vector<float>& v, vector<float>& v1, vector<float>& v2);  // Steffensen


#endif